package net.mready.android.fragment.demos.frg;

import android.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import net.mready.android.fragment.demos.MultipleFragments;

public class ItemListFragment extends ListFragment {

    public static interface OnItemSelectedListener {
        void onItemSelect(int position);
    }

    private MultipleFragments.Item[] items;

    OnItemSelectedListener listener;

    public ItemListFragment(MultipleFragments.Item[] items) {
        this.items = items;
    }

    public ItemListFragment() {

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onItemSelect(position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnItemSelectedListener) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null)
            items = (MultipleFragments.Item[]) savedInstanceState.getSerializable("items");

        setListAdapter(new ArrayAdapter<MultipleFragments.Item>(getActivity(), R.layout.simple_list_item_1, items));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("items", items);
    }
}
