package net.mready.android.fragment.demos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import net.mready.android.fragment.R;
import net.mready.android.fragment.demos.frg.TextFragment;

public class CustomAnimation extends FragmentActivity {

    private int stackLevel = 0;

    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.backstack);

        checkBox = (CheckBox) findViewById(R.id.checkBox);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment();
            }
        });

        addFragment();
    }

    private void addFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter,
                                                R.anim.fragment_slide_left_exit,
                                                R.anim.fragment_slide_right_enter,
                                                R.anim.fragment_slide_right_exit);

        fragmentTransaction.replace(R.id.llFragment, new TextFragment(++stackLevel));

        if (checkBox.isChecked())
            fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

}
