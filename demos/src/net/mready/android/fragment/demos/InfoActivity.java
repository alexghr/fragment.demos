package net.mready.android.fragment.demos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.TextView;

public class InfoActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView textView = new TextView(this);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        Intent intent = getIntent();
        String info = intent.getStringExtra("info");
        if (info != null)
            textView.setText(info);

        setContentView(textView);
    }
}