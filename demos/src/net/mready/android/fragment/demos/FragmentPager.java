package net.mready.android.fragment.demos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import net.mready.android.fragment.R;
import net.mready.android.fragment.demos.frg.TextFragment;

import java.util.ArrayList;
import java.util.List;

public class FragmentPager extends FragmentActivity {

    private ViewPager viewPager;
    private LinearLayout linearLayout;

    private List<TextFragment> fragments;
    private List<ImageView> dots;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pager);

        fragments = new ArrayList<TextFragment>();
        dots = new ArrayList<ImageView>();

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        initPager();
    }

    private void initPager() {
        addFragmentToPager();
        addFragmentToPager();
        addFragmentToPager();
        addFragmentToPager();
        addFragmentToPager();

        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return fragments.get(i);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            int previousPage;

            @Override
            public void onPageSelected(int position) {
                dots.get(previousPage).setImageResource(R.drawable.dot_off);
                dots.get(position).setImageResource(R.drawable.dot_on);

                previousPage = position;
            }
        });
    }

    private void addFragmentToPager() {
        fragments.add(new TextFragment(fragments.size() + 1));

        ImageView dot = new ImageView(this);
        dot.setImageResource(R.drawable.dot_off);
        dots.add(dot);
        linearLayout.addView(dot);

        if (dots.size() == 1)
            dot.setImageResource(R.drawable.dot_on);
    }

}