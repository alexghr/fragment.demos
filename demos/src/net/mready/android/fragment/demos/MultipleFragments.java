package net.mready.android.fragment.demos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import net.mready.android.fragment.R;
import net.mready.android.fragment.demos.frg.ExpandedItemFragment;
import net.mready.android.fragment.demos.frg.ItemListFragment;

import java.io.Serializable;

public class MultipleFragments extends FragmentActivity implements ItemListFragment.OnItemSelectedListener {

    public static class Item implements Serializable {
        public String title, info;

        public Item(String title, String info) {
            this.title = title;
            this.info = info;
        }

        @Override
        public String toString() {
            return title;
        }
    }

    private Item[] items = {
            new Item("A", "info 1"),
            new Item("B", "info 2"),
            new Item("C", "info 3"),
            new Item("D", "info 4"),
            new Item("E", "info 5"),
            new Item("F", "info 6"),
            new Item("G", "info 7"),
            new Item("H", "info 8"),
            new Item("I", "info 9"),
            new Item("J", "info 10"),
    };

    private int lastItemSelected;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multiple_fragments);


        ItemListFragment itemListFragment = new ItemListFragment(items);
        getSupportFragmentManager().beginTransaction().add(R.id.llItemList, itemListFragment).commit();

        Display display = getWindowManager().getDefaultDisplay();
        if (display.getWidth() >= 480) {
            getSupportFragmentManager().beginTransaction().add(R.id.llItemInfo, new ExpandedItemFragment(), "info").commit();
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("info");
            if (fragment != null)
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                                   ViewGroup.LayoutParams.MATCH_PARENT);
            findViewById(R.id.llItemList).setLayoutParams(layoutParams);
            findViewById(R.id.llItemList).requestLayout();
        }
    }

    @Override
    public void onItemSelect(int position) {
        lastItemSelected = position;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("info");
        if (fragment != null) {
            ((ExpandedItemFragment) fragment).setInfo(items[position].info);
        } else {
            Intent intent = new Intent(this, InfoActivity.class);
            intent.putExtra("info", items[position].info);

            startActivity(intent);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null)
            lastItemSelected = savedInstanceState.getInt("last");

        onItemSelect(lastItemSelected);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("last", lastItemSelected);
    }
}