package net.mready.android.fragment;

import android.R;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import net.mready.android.fragment.demos.BackStack;
import net.mready.android.fragment.demos.CustomAnimation;
import net.mready.android.fragment.demos.FragmentPager;
import net.mready.android.fragment.demos.MultipleFragments;

public class DemoList extends ListActivity {

    private class Demo {
        public Class<?> demo;

        public Demo(Class<?> demo) {
            this.demo = demo;
        }

        @Override
        public String toString() {
            return demo.getSimpleName();
        }
    }

    private Demo[] demos;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        demos = new Demo[] {
                new Demo(BackStack.class),
                new Demo(CustomAnimation.class),
                new Demo(MultipleFragments.class),
                new Demo(FragmentPager.class),
        };

        setListAdapter(new ArrayAdapter<Demo>(this, R.layout.simple_list_item_1, demos));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        startActivity(new Intent(this, demos[position].demo));
    }
}